package pl.com.rst.pocket;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity{

    private static final String VALID_USER_NAME = "admin";
    private static final String VALID_PASSWORD = "admin";

    @Bind(R.id.userName)
    EditText userName;
    @Bind(R.id.password)
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.loginBtn)
    void loginClick() {
        String un = userName.getText().toString();
        String pass = password.getText().toString();

        if (isEmpty(userName)){
            userName.setError(getString(R.string.no_user_text));
            return;
        }
        if(isEmpty(password)) {
            password.setError(getString(R.string.no_password_text));
            return;
        }

        if(isLoginValid(un, pass)) {
            proceed();
        }
        else {
            userName.setError(getString(R.string.bad_params_text));
            password.setError(getString(R.string.bad_params_text));
            Toast.makeText(this, R.string.login_fail_text, Toast.LENGTH_LONG).show();
        }
    }

    private boolean isEmpty(EditText input) {
        return input.getText().length() == 0;
    }

    private boolean isLoginValid(String userName, String password){
        return userName.equals(VALID_USER_NAME) &&
                password.equals(VALID_PASSWORD);
    }

    private void proceed(){
        Toast.makeText(this, "UDAŁO SIĘ!", Toast.LENGTH_LONG).show();
    }
}
